var MaxApp = angular.module('MaxApp', ['ngRoute']);
MaxApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  $locationProvider.hashPrefix('');
  $locationProvider.html5Mode(true);
  $routeProvider
  .when("/home", {
    templateUrl : "views/home.html"
  })
  .when("/charts", {
    templateUrl : "views/pieChart.html"
  });
}]);
