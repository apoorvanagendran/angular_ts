/*global angular */
(function () {
  'use strict';
  MaxApp.directive("commentsDirective", 
  	function() {
    return {
        templateUrl : "views/comments.html"
    };
});
})();