(function () {
  'use strict';
  MaxApp.controller('likesController',
   function($scope) {
   $scope.count = 15;
   var max = $scope.count + 1;
   var min = $scope.count - 1;

  $scope.increment = function() {
    if ($scope.count >= max) { return; }
    $scope.count++;
  };
  $scope.decrement = function() {
    if ($scope.count <= min) { return; }
    $scope.count--;
  };
});
})();