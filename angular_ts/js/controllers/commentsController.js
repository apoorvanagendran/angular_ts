/*global angular */
(function () {
  'use strict';
  MaxApp.controller('commentsCtrl',
   function($scope){
	$scope.comments = [];

    $scope.submit = function() {
      $scope.comments.push({
        text: $scope.text
      });
      $scope.name = '';
      return $scope.text = '';
    };

    $scope.approve = function(comment) {
      return comment.approved = true;
    };
    return $scope.drop = function(comment) {
      return $scope.comments.splice($scope.comments.indexOf(comment), 1);
    };

});
})();
